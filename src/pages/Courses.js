import CourseCard from '../components/CourseCard';
import { useEffect, useState } from 'react'
// import coursesData from '../data/coursesData';

export default function Courses() {

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([])
	
	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() =>{

		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
			.then(res => res.json())
			.then(data =>{

				console.log(data);
				setCourses(data.map(course => {
		return (
			<CourseCard key={ course._id } course={ course } />
		)
	})
)
			})
	}, [])

	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={ course.id } course={ course } />
	// 	)
	// })

	return (

		<>
			{ courses }
		</>

	)
}

/*
	course = {
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		price: 45000,
		onOffer: true
	}

*/
